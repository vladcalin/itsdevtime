import pytest

from itsdevtime.builder import DockerBuilder


@pytest.mark.parametrize('service_name, expected_tag', [
    ('backend', 'backend:dev'),
])
def test_get_tag(service_name, expected_tag):
    assert DockerBuilder(service_name, None).get_docker_tag() == expected_tag
