import logging
import os.path
import subprocess


class DockerBuilder:
    """
    # build the <service>:dev image with this dockerfile
    dockerfile: path/to/dockerfile/relative/to/source

    # commands to execute before running the docker build
    local:
    - yarn
    - stuff

    """

    def __init__(self, service_name, service_config):
        self.service_name = service_name
        self.service_config = service_config

    def build(self):
        build_cfg = self.service_config['build']
        if 'dockerfile' in build_cfg:

            if 'local' in build_cfg:
                for cmd in build_cfg['local']:
                    logging.info(f'Running pre-docker-build command: {cmd}')
                    subprocess.check_call(cmd, cwd=self.service_config['__source'])

            self.build_with_dockerfile(build_cfg)

    def build_with_dockerfile(self, build_cfg):
        logging.info('Building with dockerfile')
        cwd = self.service_config.get('__source')
        dockerfile_path = os.path.join(cwd, build_cfg['dockerfile'])

        subprocess.check_call([
            'docker', 'build', '-t', self.get_docker_tag(), '-f', dockerfile_path, cwd
        ])

    def get_docker_tag(self):
        return f'{self.service_name}:dev'
