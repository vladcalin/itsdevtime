from itsdevtime.builder.docker import DockerBuilder


def get_builder_class(type):
    if type == 'docker':
        return DockerBuilder
