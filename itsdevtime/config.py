import json
import logging
import os.path

from itsdevtime.service import ServiceConfig, resolve_content, is_remote, ServiceConfigError


class Config:
    def __init__(self):
        self.ensure_config_file()
        self.content = self.load_content()

        self._current_profile = self.content.get('active_profile')
        self.profiles = self.content['profiles']

    @property
    def current_profile(self):
        if self._current_profile not in self.profiles:
            return None
        return self.profiles[self._current_profile]

    def get_config_path(self):
        return os.path.expanduser('~/.itsdevtime/config')

    def ensure_config_file(self):
        cfg_path = self.get_config_path()

        dirname = os.path.dirname(cfg_path)
        os.makedirs(dirname, exist_ok=True)

        if not os.path.exists(cfg_path):
            with open(cfg_path, 'w') as f:
                json.dump(self.get_default_config(), f)

    @staticmethod
    def get_default_config():
        return {
            'profiles': {},
            'active_profile': None
        }

    def get_content(self):
        return self.content

    def load_content(self):
        with open(self.get_config_path(), 'r') as f:
            return json.load(f)

    def persist_content(self):
        with open(self.get_config_path(), 'w') as f:
            json.dump({
                'profiles': self.profiles,
                'active_profile': self._current_profile
            }, f)

    def has_profile(self, name):
        return name in self.profiles

    def create_profile(self, name):
        self.profiles[name] = {}
        if not self._current_profile:
            self._current_profile = name
        return self.profiles[name]

    def delete_profile(self, name):
        if self._current_profile == name:
            self._current_profile = None
        del self.profiles[name]

    def set_current_profile(self, name):
        self._current_profile = name

    def add_service(self, name, data, source, profile_name=None):
        if not profile_name:
            profile = self.current_profile
        else:
            profile = self.profiles[profile_name]

        if 'services' not in profile:
            profile['services'] = {}
        if not is_remote(source):
            source = os.path.abspath(source)

        if len(data) == 1:
            profile['services'][name] = data[0]
            profile['services'][name]['__source'] = source
            profile['services'][name]['__service'] = name
        else:
            if any('name' not in d for d in data):
                raise ServiceConfigError('Not all service flavors have a specified name. '
                                         'This is required if there are multiple flavors defined in the '
                                         'same itsdevtime.yaml')
            logging.debug(f'More flavors found for {name}')
            for flavor in data:
                final_name = f'{name}-{flavor["name"]}'
                logging.debug(f'Adding {final_name}')
                profile['services'][final_name] = flavor
                profile['services'][final_name]['__source'] = source
                profile['services'][final_name]['__service'] = name

    def remove_service(self, name):
        del self.current_profile['services'][name]

    def get_all_service_names(self):
        return list(self.current_profile['services'].keys())

    def get_service(self, name):
        return self.current_profile['services'][name]

    def refresh(self):
        sources = set()
        for pname, p in self.profiles.items():
            for s in p['services'].values():
                sources.add((pname, s['__source'], s['__service']))

        self.profiles = {}
        for profile_name in set(map(lambda x: x[0], sources)):
            self.profiles[profile_name] = {}

        for profile_name, service_source, service_name in sources:
            self.add_service_from_location(service_name, service_source, profile_name=profile_name)

    def add_service_from_location(self, name, location, profile_name=None):
        data = resolve_content(location)
        self.add_service(name, data, source=location, profile_name=profile_name)
