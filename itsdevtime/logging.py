import logging
import sys

import click
import colorlog


def init_logging(level=logging.INFO):
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(
        '%(log_color)s%(message)s'))

    logging.basicConfig(
        level=level,
        stream=sys.stdout
    )
    logger = logging.getLogger()
    logger.handlers = []
    logger.addHandler(handler)


def _log_color(txt, color):
    click.echo(click.style(txt, fg=color))


def log(txt):
    _log_color(txt, 'yellow')


def log_success(txt):
    logging.info(txt)
    # _log_color(txt, 'green')


def log_error(txt):
    logging.error(txt)
    # _log_color(txt, 'red')


def log_debug(txt):
    logging.debug(txt)
