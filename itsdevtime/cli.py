import json
import logging
import sys
import time

import click

from itsdevtime.builder import get_builder_class
from itsdevtime.config import Config
from itsdevtime.logging import log_error, init_logging
from itsdevtime.service import resolve_content, ServiceConfigError

init_logging()


@click.group()
def main():
    pass


@main.command('version')
def main_version():
    from itsdevtime import __VERSION__
    click.echo(__VERSION__)


@main.command('help')
@click.pass_context
def main_help(context):
    click.echo(main.get_help(context))


@main.group('config')
def config():
    pass


@config.command('view')
def config_view():
    click.echo(json.dumps(Config().get_content(), indent=2))


@config.command('refresh')
def config_refresh():
    c = Config()
    c.refresh()
    c.persist_content()


@main.group('profile')
def profile():
    pass


@profile.command('create')
@click.argument('name')
def profile_create(name):
    c = Config()
    if c.has_profile(name):
        log_error(f'Profile {name} already exists!')
        sys.exit(-1)

    c.create_profile(name)
    c.persist_content()


@profile.command('delete')
@click.argument('name')
def profile_delete(name):
    c = Config()
    if not c.has_profile(name):
        log_error(f'Profile {name} does not exist!')
        sys.exit(-1)

    c.delete_profile(name)
    c.persist_content()


@profile.command('set-current')
@click.argument('name')
def profile_set_current(name):
    c = Config()
    if not c.has_profile(name):
        log_error(f'Profile {name} does not exist!')
        sys.exit(-1)

    c.set_current_profile(name)
    c.persist_content()


@main.group('service')
def service():
    pass


@service.command('add')
@click.argument('name')
@click.argument('location')
def service_add(name, location):
    c = Config()
    if not c._current_profile:
        log_error('No current profile selected')
        sys.exit(-1)

    c.add_service_from_location(name, location)
    c.persist_content()


@service.command('set-source')
@click.argument('name')
@click.argument('location')
def service_set_source(name, location):
    c = Config()
    if not c._current_profile:
        log_error('No current profile selected')
        sys.exit(-1)

    c.remove_service(name)
    c.add_service_from_location(name, location)
    c.persist_content()


@main.command('build')
@click.argument('service_name', nargs=-1)
def build(service_name):
    c = Config()
    if not service_name:
        services = c.get_all_service_names()
    else:
        services = service_name

    logging.info(f'Building services: {", ".join(services)}')

    def get_service_build_type(name, config):
        if config.get('type') == 'docker':
            if 'image' in config:
                logging.warning(f'Skipping {name} because image is already present')
                return None
            return 'docker'
        raise ValueError('Unable to determine the necessary build type for "{}"'.format(name))

    for svc_name in services:
        service_config = c.get_service(svc_name)
        logging.info(f'Inspecting {svc_name}')
        service_type = get_service_build_type(svc_name, service_config)
        if not service_type:
            continue
        logging.info(f'Building {svc_name}')
        _start = time.time()
        builder = get_builder_class(service_type)(svc_name, service_config)
        builder.build()
        logging.info(f'Building done in {time.time() - _start:.2f}s')
