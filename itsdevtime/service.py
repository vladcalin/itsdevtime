import logging
import os.path
from urllib.parse import urlparse

import requests
import yaml


class ServiceConfigError(Exception):
    pass


class ServiceConfig:
    def __init__(self, type, build, run):
        self.type = type
        self.build = build
        self.run = run

    @classmethod
    def from_dict(cls, d):
        return cls(d['type'], d.get('build', {}), d.get('run', {}))

    def to_dict(self):
        return {
            'type': self.type,
            'build': self.build,
            'run': self.run
        }


def get_user_agent():
    from itsdevtime import __VERSION__
    return f'itsdevtime {__VERSION__} <https://gitlab.com/vladcalin/itsdevtime>'


def resolve_remote_content(location):
    resp = requests.get(location, headers={'User-Agent': get_user_agent()})
    logging.debug(f'Downloading {location}')
    return yaml.load_all(resp.content, Loader=yaml.SafeLoader)


def is_remote(from_location):
    return bool(urlparse(from_location).scheme)


def resolve_local_content(location):
    abspath = os.path.abspath(location)
    service_def_path = os.path.join(abspath, 'itsdevtime.yaml')
    if not os.path.isfile(service_def_path):
        raise ServiceConfigError(f'{service_def_path} does not exist')

    with open(service_def_path, 'r') as f:
        return list(yaml.load_all(f, Loader=yaml.Loader))


def resolve_content(from_location):
    if is_remote(from_location):
        data = resolve_remote_content(from_location)
    else:  # is local
        data = resolve_local_content(from_location)
    return list(data)
